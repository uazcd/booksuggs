﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

namespace AmazonConnector
{
    /// <summary>
    /// Simplifies the results returned from Amazon queries.
    /// </summary>
    public class AmazonResult
    {
        public string ISBN { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Publisher { get; set; }
        public string Year { get; set; }
        public Uri NavigateURL { get; set; }
        public string Price { get; set; }

        public string[] EISBN { get; set; }
    }

    //see http://stackoverflow.com/questions/436801/could-someone-provide-a-c-sharp-example-using-itemsearch-from-amazon-web-service
    /// <summary>
    /// Simple ISBN based or attribute based query.  ISBN does a direct lookup.  The 
    /// The rest are 'and' together the criteriua
    /// </summary>
    /// 
    public class AmazonQuery
    {
        
        public string Author { get; set; }
        public string ISBN { get; set; }
        public string Publisher { get; set; }
        public string Title { get; set; }
        public int Year { get; set; }

        /// <summary>
        /// Runs a search directly
        /// </summary>
        /// <returns>A list of simple result objects</returns>
        public IEnumerable<AmazonConnector.AmazonResult> Search()
        {
            
            AWSE.AWSECommerceServicePortTypeClient myClient = GetClient();//need to create the client and sign its requests
            AWSE.ItemLookup lookup = new AWSE.ItemLookup();

            //Create both search requests and itel lookup requests
            AWSE.ItemSearchRequest searchRequest = new AWSE.ItemSearchRequest();
            AWSE.ItemSearch search = new AWSE.ItemSearch();
            
            
            searchRequest.SearchIndex="Books";
            searchRequest.Condition = AWSE.Condition.All; 
            searchRequest.ResponseGroup = new string[] { "ItemAttributes" };
            
            
            AWSE.ItemLookupRequest lookupRequest = new AWSE.ItemLookupRequest();

            lookupRequest.SearchIndex="Books";
            lookupRequest.Condition = AWSE.Condition.All;
            lookupRequest.ResponseGroup = new string[] { "ItemAttributes" };

            //create the response objects
            AWSE.ItemLookupResponse lookupResponse;
            AWSE.ItemSearchResponse searchResponse;
            AWSE.Items[] responseItems;
            //get the associate tag
            string assocID = Properties.Settings.Default.AWSAccountID;

            search.AssociateTag = assocID;
            lookup.AssociateTag = assocID;

            
            //can't do an ISBN search or author search:  it's one or other
            if (!string.IsNullOrWhiteSpace(ISBN))
            {
                lookupRequest.IdType = AWSE.ItemLookupRequestIdType.ISBN;
                lookupRequest.ItemId = new string[] { ISBN.Replace("-", "") };


                lookup.Request = new AWSE.ItemLookupRequest[] { lookupRequest };
                
                lookupResponse = myClient.ItemLookup(lookup);
                responseItems = lookupResponse.Items;
              
            }
            else // do an author search
            {
                //chain together various criteria
                if (!string.IsNullOrWhiteSpace(Author))
                {
                    
                    searchRequest.Author = Author;
                }
                if(!string.IsNullOrWhiteSpace(Title))
                {
                    searchRequest.Title = Title;

                }
                if (!string.IsNullOrWhiteSpace(Publisher))
                {
                    searchRequest.Publisher = Publisher;

                }
                search.Request = new AWSE.ItemSearchRequest[] { searchRequest };
                searchResponse = myClient.ItemSearch(search);
                responseItems = searchResponse.Items;
            }
            //at this point we've either done a lookup or a search

            //now we convert the search results into something more tractable

            if (responseItems.Length > 0 && responseItems[0].Item != null && responseItems[0].Item.Length > 0)
            {
                //bit of LINQ to parcel stuff up
                var results = from i in responseItems[0].Item
                              select new AmazonResult
                              {
                                  ISBN = i.ItemAttributes.ISBN,
                                  EISBN = i.ItemAttributes.EISBN,
                                  Name = i.ItemAttributes.Author!=null?i.ItemAttributes.Author.Aggregate( (a,b) => a + ", " + b):"",
                                  Publisher = string.Join(", ", i.ItemAttributes.Publisher),
                                  Title = string.Join(", ", i.ItemAttributes.Title),
                                  Year = i.ItemAttributes.ReleaseDate??"",
                                  NavigateURL =  new Uri(i.DetailPageURL),
                                  Price =i.ItemAttributes.ListPrice!=null?i.ItemAttributes.ListPrice.CurrencyCode + string.Format("{0:0.00}", 
                                  double.Parse(i.ItemAttributes.ListPrice.Amount)/100):""
                              };
                return results;
            }
            else
                return null;
            
        }
        /// <summary>
        /// Returns a port-type web service client for querying Amazon.  
        /// Both access key and secret key need to be stored in the app.config
        /// </summary>
        /// <returns></returns>
        private static AWSE.AWSECommerceServicePortTypeClient GetClient()
        {
            //both these values need to be got from the app.config.
            string accessKeyId = Properties.Settings.Default.AWSAccessKeyId;
            string secretKey = Properties.Settings.Default.AWSSecretKey;



            BasicHttpBinding binding = new BasicHttpBinding(BasicHttpSecurityMode.Transport);

            binding.MaxReceivedMessageSize = int.MaxValue;

            AWSE.AWSECommerceServicePortTypeClient amazonClient = new AWSE.AWSECommerceServicePortTypeClient(
            binding,
            new EndpointAddress("https://webservices.amazon.com/onca/soap?Service=AWSECommerceService"));

            // add authentication to the ECS client
            amazonClient.ChannelFactory.Endpoint.Behaviors.Add(new AmazonSigningEndpointBehavior(accessKeyId, secretKey));
            return amazonClient;
        }
    }
    //various support classes for Amazon follow - I didn't write these, simply Googled them
    public class AmazonHeader : MessageHeader
    {
        private string name;
        private string value;

        public AmazonHeader(string name, string value)
        {
            this.name = name;
            this.value = value;
        }

        public override string Name { get { return name; } }
        public override string Namespace { get { return "http://security.amazonaws.com/doc/2007-01-01/"; } }

        protected override void OnWriteHeaderContents(XmlDictionaryWriter xmlDictionaryWriter, MessageVersion messageVersion)
        {
            xmlDictionaryWriter.WriteString(value);
        }
    }

    public class AmazonSigningMessageInspector : IClientMessageInspector
    {
        private string accessKeyId = "";
        private string secretKey = "";

        public AmazonSigningMessageInspector(string accessKeyId, string secretKey)
        {
            this.accessKeyId = accessKeyId;
            this.secretKey = secretKey;
        }

        public object BeforeSendRequest(ref Message request, IClientChannel channel)
        {
            // prepare the data to sign
            string operation = Regex.Match(request.Headers.Action, "[^/]+$").ToString();
            DateTime now = DateTime.UtcNow;
            string timestamp = now.ToString("yyyy-MM-ddTHH:mm:ssZ");
            string signMe = operation + timestamp;
            byte[] bytesToSign = Encoding.UTF8.GetBytes(signMe);

            // sign the data
            byte[] secretKeyBytes = Encoding.UTF8.GetBytes(secretKey);
            HMAC hmacSha256 = new HMACSHA256(secretKeyBytes);
            byte[] hashBytes = hmacSha256.ComputeHash(bytesToSign);
            string signature = Convert.ToBase64String(hashBytes);

            // add the signature information to the request headers
            request.Headers.Add(new AmazonHeader("AWSAccessKeyId", accessKeyId));
            request.Headers.Add(new AmazonHeader("Timestamp", timestamp));
            request.Headers.Add(new AmazonHeader("Signature", signature));

            return null;
        }

        public void AfterReceiveReply(ref Message reply, object correlationState) { }
    }

    public class AmazonSigningEndpointBehavior : IEndpointBehavior
    {
        private string accessKeyId = "";
        private string secretKey = "";

        public AmazonSigningEndpointBehavior(string accessKeyId, string secretKey)
        {
            this.accessKeyId = accessKeyId;
            this.secretKey = secretKey;
        }

        public void ApplyClientBehavior(ServiceEndpoint serviceEndpoint, ClientRuntime clientRuntime)
        {
            clientRuntime.MessageInspectors.Add(new AmazonSigningMessageInspector(accessKeyId, secretKey));
        }

        public void ApplyDispatchBehavior(ServiceEndpoint serviceEndpoint, EndpointDispatcher endpointDispatcher) { return; }
        public void Validate(ServiceEndpoint serviceEndpoint) { return; }
        public void AddBindingParameters(ServiceEndpoint serviceEndpoint, BindingParameterCollection bindingParameters) { return; }
    }

   
}
