﻿Imports System.Web, System.Net, System.Xml.Serialization
Public Class AlephQuery
    Private mTitle As String
    Public Property Title() As String
        Get
            Return mTitle
        End Get
        Set(ByVal value As String)
            mTitle = value
        End Set
    End Property

    Private mAuthor As String
    Public Property Author() As String
        Get
            Return mAuthor
        End Get
        Set(ByVal value As String)
            mAuthor = value
        End Set
    End Property

    Private mPublisher As String
    Public Property Publisher() As String
        Get
            Return mPublisher
        End Get
        Set(ByVal value As String)
            mPublisher = value
        End Set
    End Property

    Private mYear As Integer
    Public Property Year() As Integer
        Get
            Return mYear
        End Get
        Set(ByVal value As Integer)
            mYear = value
        End Set
    End Property
    Private mISBN As String
    Public Property ISBN() As String
        Get
            If Not String.IsNullOrEmpty(mISBN) Then
                Return """" & mISBN & """"
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            mISBN = value
        End Set
    End Property
    Private msAlephBase As String
    Public Property AlephBase() As String
        Get
            Return msAlephBase
        End Get
        Set(ByVal value As String)
            msAlephBase = value
        End Set
    End Property

    Property ServerName As String
    Property ServerPort As String = ""

    Private Function BuildCriteria(ByVal crits As Dictionary(Of String, String)) As String
        Dim sRes As String
        For Each crit As String In crits.Keys
            If Not String.IsNullOrEmpty(crits(crit)) Then
                If sRes = "" Then
                    sRes = crit & "=" & crits(crit)
                Else
                    sRes += "+AND+" + crit & "=" & crits(crit)
                End If
            End If
        Next
        Return sRes
    End Function

    Friend Function Search() As SearchResult

        Dim sb As New System.Text.StringBuilder
        sb.Append("http://")
        sb.Append(ServerName)
        If Not String.IsNullOrWhiteSpace(ServerPort) Then
            sb.Append(":" & ServerPort)
        End If
        sb.Append("/X?")
        sb.Append("op=find")
        'sb.Append("&code=wrd")
        sb.Append("&request=")

        Dim Criteria As New Dictionary(Of String, String)

        Criteria("wau") = Author
        Criteria("wti") = Title
        Criteria("pub") = Publisher
        Criteria("isbn") = ISBN
        sb.Append(BuildCriteria(Criteria))

        sb.Append("&base=")
        sb.Append(AlephBase)

        Dim wc As WebRequest = WebRequest.Create(sb.ToString())


        Dim wr As WebResponse = wc.GetResponse()
        Dim xds As New XmlSerializer(GetType(SearchResult))
        Dim res As SearchResult = xds.Deserialize(wr.GetResponseStream())
        res.ServerName = ServerName
        res.ServerPort = ServerPort
        Return res

    End Function

End Class

