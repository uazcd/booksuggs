﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
    <xsl:output method="xml" indent="yes"/>

  
    <xsl:template match="present">
      <Collection>
            <xsl:apply-templates/>
      </Collection>
    </xsl:template>

  <xsl:template match="record">
    <Record>        
      <xsl:apply-templates/>
    </Record>
  </xsl:template>
  
  <xsl:template match="record_header|set_entry">
      <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="doc_number">
    <DocNumber>
      <xsl:value-of select="."/>
    </DocNumber>
  </xsl:template>
  
  <xsl:template match="metadata">
    <xsl:apply-templates/>
  </xsl:template>
  
  <xsl:template match="oai_marc">
    <xsl:apply-templates/>
  </xsl:template>
   
  <xsl:template match="varfield[@id='100']">
    <Name>
      <xsl:value-of select="normalize-space(concat(.,/subfield/@value))"/>
    </Name>
  </xsl:template>
  
  <xsl:template match="varfield[@id='020']">
    <ISBN>
      <xsl:apply-templates/>
    </ISBN>
  </xsl:template>
  
  <xsl:template match="varfield[@id='020']/subfield[@label='a']">
    <!-- strip out all non-numeric characters-->
    <xsl:value-of select="translate(., translate(., '0123456789', ''), '')"/>

  </xsl:template>
                
  <!--
  <xsl:template match="varfield[@id='100']/subfield">
      <xsl:value-of select="."/>
  </xsl:template>
    -->
  <xsl:template match="varfield[@id='260']">
    <Publisher>
      <xsl:apply-templates/>
    </Publisher>
  </xsl:template>
  
  <xsl:template match="varfield[@id='260']/subfield">
    <xsl:value-of select="."/>
  </xsl:template>
  
  
  <xsl:template match="varfield[@id='260']/subfield[@label='c']">
    <PubDate><xsl:value-of select="."/></PubDate>
  </xsl:template>
  <!--
  <xsl:template match="varfield[@id='245']/subfield">
      <xsl:value-of select="."/>
  </xsl:template>
-->
  <xsl:template match="varfield[@id='245']">
    <Title><xsl:value-of select="normalize-space(concat(.,/subfield/@value))"/></Title>
  </xsl:template>
  
    <xsl:template match="text() | @*">
    </xsl:template>

</xsl:stylesheet>
