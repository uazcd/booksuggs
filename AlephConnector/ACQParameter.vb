﻿Imports System.Xml.Serialization, System.Net
Imports System.Xml.Linq
Imports System.Reflection

Imports MARC4J, MARC4J.Net
Imports System.IO

Public Class ACQParameter
    Private msAllowed As String
    <XmlAttribute("allowed")>
    Public Property Allowed() As String
        Get
            Return msAllowed
        End Get
        Set(ByVal value As String)
            msAllowed = value
        End Set
    End Property

    Private msReason As String
    <XmlElement("reason")>
    Public Property Reason() As String
        Get
            Return msReason
        End Get
        Set(ByVal value As String)
            msReason = value
        End Set
    End Property

    Private msOrderGroup As String
    <XmlElement("order-group")>
    Public Property OrderGroup() As String
        Get
            Return msOrderGroup
        End Get
        Set(ByVal value As String)
            msOrderGroup = value
        End Set
    End Property

    Private msAuthor As String
    <XmlElement("author")>
    Public Property Author() As String
        Get
            Return msAuthor
        End Get
        Set(ByVal value As String)
            msAuthor = value
        End Set
    End Property

    Private msTitle As String
    <XmlElement("title")>
    Public Property Title() As String
        Get
            Return msTitle
        End Get
        Set(ByVal value As String)
            msTitle = value
        End Set
    End Property

    Private msAddAuthor As String
    <XmlElement("add-author")>
    Public Property AddAuthor() As String
        Get
            Return msAddAuthor
        End Get
        Set(ByVal value As String)
            msAddAuthor = value
        End Set
    End Property

    Private msEdition As String
    <XmlElement("edition")>
    Public Property Edition() As String
        Get
            Return msEdition
        End Get
        Set(ByVal value As String)
            msEdition = value
        End Set
    End Property

    Private msPubPlace As String
    <XmlElement("publication-place")>
    Public Property PublicationPlace() As String
        Get
            Return msPubPlace
        End Get
        Set(ByVal value As String)
            msPubPlace = value
        End Set
    End Property

    Private msPublisher As String
    <XmlElement("publisher")>
    Public Property Publisher() As String
        Get
            Return msPublisher
        End Get
        Set(ByVal value As String)
            msPublisher = value
        End Set
    End Property

    Private msPubYear As String
    <XmlElement("publication-year")>
    Public Property PubYear() As String
        Get
            Return msPubYear
        End Get
        Set(ByVal value As String)
            msPubYear = value
        End Set
    End Property

    Private msISBN As String
    <XmlElement("ISBN")>
    Public Property ISBN() As String
        Get
            Return msISBN
        End Get
        Set(ByVal value As String)
            msISBN = value
        End Set
    End Property

    Private msSeries As String
    <XmlElement("series")>
    Public Property Series() As String
        Get
            Return msSeries
        End Get
        Set(ByVal value As String)
            msSeries = value
        End Set
    End Property

    Private msInfoSource As String
    <XmlElement("info-source")>
    Public Property InfoSource() As String
        Get
            Return msInfoSource
        End Get
        Set(ByVal value As String)
            msInfoSource = value
        End Set
    End Property

End Class
