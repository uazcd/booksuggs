﻿Imports System.Xml.Serialization, System.Net

Imports MARC4J.Net

<XmlRoot("put-rec-acq")>
Public Class NewACQRequest
    Private msReplyText As String
    <XmlElement("reply-text")>
    Public Property Replytext() As String
        Get
            Return msReplyText
        End Get
        Set(ByVal value As String)
            msReplyText = value
        End Set
    End Property


    Private msReplyCode As String
    <XmlElement("reply-code")>
    Public Property ReplyCode() As String
        Get
            Return msReplyCode
        End Get
        Set(ByVal value As String)
            msReplyCode = value
        End Set
    End Property

    Private AQCParams As ACQParameter
    <XmlElement("acq_parameters")>
    Public Property ACQParameters() As ACQParameter
        Get
            Return AQCParams
        End Get
        Set(ByVal value As ACQParameter)
            AQCParams = value
        End Set
    End Property
    <XmlIgnore>
    Property ServerName As String
    <XmlIgnore>
    Property ServerPort As String
    <XmlIgnore>
    Property PatronNo As String

    Public Function Send() As String
        Dim sb As New System.Text.StringBuilder
        Dim response As Byte()
        sb.Append("http://")
        sb.Append(ServerName)
        If Not String.IsNullOrWhiteSpace(ServerPort) Then
            sb.Append(":" & ServerPort)
        End If
        sb.Append("/rest-dlf/patron/")
        sb.Append(PatronNo)

        sb.Append("/acqRequest")
        'build the XML body of the request
        Dim ser As New XmlSerializer(GetType(NewACQRequest))
        Dim encod = System.Text.Encoding.UTF8
        Dim sb2 As New System.Text.StringBuilder
        Dim sw As System.Xml.XmlWriter = System.Xml.XmlWriter.Create(sb2, New System.Xml.XmlWriterSettings With {.Encoding = encod, .Indent = True})
        ser.Serialize(sw, Me)
        Dim messageBody As String = sb2.ToString

        Using client = New System.Net.WebClient()
            response = client.UploadData(sb.ToString, "PUT", encod.GetBytes(sb2.ToString()))

        End Using
        Return response.ConvertToString()
    End Function
End Class

