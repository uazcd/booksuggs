﻿Imports System.Linq
Imports System.Xml.Linq
Imports System.Net
Public Class Session
    Private mServerName, mServerPort As String

    Public Sub New(ServerName As String, Optional ServerPort As String = "")
        mServerName = ServerName
        mServerPort = ServerPort
    End Sub


    Public Function Search(Query As AlephQuery) As SearchResult
        Query.ServerName = mServerName
        Query.ServerPort = mServerPort
        Dim MyResult As SearchResult = Query.Search()
        Return MyResult
    End Function
End Class
