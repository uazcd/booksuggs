﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:2.0.50727.5485
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict Off
Option Explicit On

Imports System.Xml.Serialization

'
'This source code was auto-generated by xsd, Version=2.0.50727.3038.
'

'''<remarks/>
<System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"),  _
 System.SerializableAttribute(),  _
 System.Diagnostics.DebuggerStepThroughAttribute(),  _
 System.ComponentModel.DesignerCategoryAttribute("code"),  _
 System.Xml.Serialization.XmlTypeAttribute(AnonymousType:=true),  _
 System.Xml.Serialization.XmlRootAttribute([Namespace]:="", IsNullable:=false)>  _
Partial Public Class Collection
    
    Private recordField() As CollectionRecord
    
    '''<remarks/>
    <System.Xml.Serialization.XmlElementAttribute("Record")>  _
    Public Property Record() As CollectionRecord()
        Get
            Return Me.recordField
        End Get
        Set
            Me.recordField = value
        End Set
    End Property
End Class

'''<remarks/>
<System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"),  _
 System.SerializableAttribute(),  _
 System.Diagnostics.DebuggerStepThroughAttribute(),  _
 System.ComponentModel.DesignerCategoryAttribute("code"),  _
 System.Xml.Serialization.XmlTypeAttribute(AnonymousType:=true)>  _
Partial Public Class CollectionRecord
    
    Private docNumberField As ULong
    
    Private iSBNField() As String
    
    Private nameField As String
    
    Private titleField As String
    
    Private publisherField As CollectionRecordPublisher
    
    '''<remarks/>
    Public Property DocNumber() As ULong
        Get
            Return Me.docNumberField
        End Get
        Set(value As ULong)
            Me.docNumberField = Value
        End Set
    End Property
    
    '''<remarks/>
    <System.Xml.Serialization.XmlElementAttribute("ISBN")>  _
    Public Property ISBN() As String()
        Get
            Return Me.iSBNField
        End Get
        Set
            Me.iSBNField = value
        End Set
    End Property
    
    '''<remarks/>
    Public Property Name() As String
        Get
            Return Me.nameField
        End Get
        Set
            Me.nameField = value
        End Set
    End Property
    
    '''<remarks/>
    Public Property Title() As String
        Get
            Return Me.titleField
        End Get
        Set
            Me.titleField = value
        End Set
    End Property
    
    '''<remarks/>
    Public Property Publisher() As CollectionRecordPublisher
        Get
            Return Me.publisherField
        End Get
        Set
            Me.publisherField = value
        End Set
    End Property
End Class

'''<remarks/>
<System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.3038"),  _
 System.SerializableAttribute(),  _
 System.Diagnostics.DebuggerStepThroughAttribute(),  _
 System.ComponentModel.DesignerCategoryAttribute("code"),  _
 System.Xml.Serialization.XmlTypeAttribute(AnonymousType:=true)>  _
Partial Public Class CollectionRecordPublisher
    
    Private pubDateField As String
    
    Private textField() As String
    
    '''<remarks/>
    Public Property PubDate() As String
        Get
            Return Me.pubDateField
        End Get
        Set
            Me.pubDateField = value
        End Set
    End Property
    
    '''<remarks/>
    <System.Xml.Serialization.XmlTextAttribute()>  _
    Public Property Text() As String()
        Get
            Return Me.textField
        End Get
        Set
            Me.textField = value
        End Set
    End Property
End Class
