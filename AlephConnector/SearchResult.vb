﻿Imports System.Xml.Serialization, System.Net
Imports System.Xml.Linq
Imports System.Reflection

Imports MARC4J, MARC4J.Net
Imports System.IO

<XmlRoot(ElementName:="find")>
Public Class SearchResult
    Private mSetNumber As Integer
    Private mServerName As String
    Public Property ServerName() As String
        Get
            Return mServerName
        End Get
        Set(ByVal value As String)
            mServerName = value
        End Set
    End Property

    <XmlElement("set_number")>
    Public Property SetNumber() As Integer
        Get
            Return mSetNumber
        End Get
        Set(ByVal value As Integer)
            mSetNumber = value
        End Set
    End Property

    Private mNoRecords As Integer
    <XmlElement("no_records")>
    Public Property NoRecords() As Integer
        Get
            Return mNoRecords
        End Get
        Set(ByVal value As Integer)
            mNoRecords = value
        End Set
    End Property
    <XmlElement("no_entries")>
    Private mNoEntries As Integer
    Public Property NoEnties() As Integer
        Get
            Return mNoEntries
        End Get
        Set(ByVal value As Integer)
            mNoEntries = value
        End Set
    End Property
    Private mSessionID As String
    <XmlElement("session-id")>
    Public Property SessionID() As String
        Get
            Return mSessionID
        End Get
        Set(ByVal value As String)
            mSessionID = value
        End Set
    End Property

    Private mServerPort As String = ""
    Public Property ServerPort() As String
        Get
            Return mserverPort
        End Get
        Set(ByVal value As String)
            mserverPort = value
        End Set
    End Property



    ''' <summary>
    ''' Gets the details from the basic search result.  Call this after doing a search
    ''' </summary>
    ''' <returns>'collection' object containing search records</returns>
    ''' <remarks></remarks>
    Public Function DrillDown() As collection
        Dim sb As New System.Text.StringBuilder
        sb.Append("http://")
        sb.Append(ServerName)
        If ServerPort <> "" Then
            sb.Append(":")
            sb.Append(ServerPort)
        End If
        sb.Append("/X?")
        sb.Append("op=present")
        sb.Append("&set_number=")
        sb.Append(SetNumber)
        sb.Append("&format=marc")
        sb.Append("&set_entry=000000001-1000000000")

        Dim transformer = New System.Xml.Xsl.XslCompiledTransform(True)
        Dim assemblyLocn = New System.Uri(Assembly.GetExecutingAssembly.CodeBase)
        Dim sAppPath As String = Path.GetDirectoryName(assemblyLocn.LocalPath)


        transformer.Load(My.Computer.FileSystem.CombinePath(sAppPath, "MARCSimple.xslt"))
        Dim sbOutput As New System.Text.StringBuilder
        Dim xs As New Xml.XmlWriterSettings
        xs.Indent = True

        Dim outputWriter As Xml.XmlWriter = Xml.XmlWriter.Create(sbOutput, xs)

        transformer.Transform(sb.ToString, outputWriter)

        Dim deser As New XmlSerializer(GetType(collection))
        Dim coll As collection = deser.Deserialize(New StringReader(sbOutput.ToString))


        Return coll
    End Function

End Class
