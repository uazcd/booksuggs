﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Ack.aspx.vb" Inherits="BookSuggestions.Ack" %>
<%@ PreviousPageType VirtualPath="~/Suggestion.aspx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h1>
        Suggestion received!</h1>
    <p>
        &nbsp;</p>
    <p>
        Thank you for making your suggestion.&nbsp;
    </p>
    <p>
        &nbsp;</p>
    <p>
        We will let you know soon what the result will be.</p>
    <p>
        <asp:Button ID="BackButton" runat="server" PostBackUrl="~/Suggestion.aspx" Text="Back to requests" />
    </p>
</asp:Content>
