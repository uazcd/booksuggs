﻿Imports System.Data.OleDb
Imports System.Net.Mail
Public Class Suggestion
    Inherits System.Web.UI.Page


    Protected Sub SubmitButton_Click(sender As Object, e As EventArgs)
        WriteAlephRequest()
        WriteAccessRequest2()
    End Sub

    Private Sub WriteAccessRequest()

        Dim dbProvider As String = "PROVIDER=Microsoft.ACE.OLEDB.12.0;"
        Dim dbsource = "Data Source = " & Server.MapPath(My.Settings.DatabaseName)

        Dim sql = "INSERT INTO Suggestions ([Requester], [Email], Dept, Barcode, Author, " &
            "Title, Pub, PubDate, ISBN1, ISBN2, IntendedLibrary, Price, OLCopies, " &
            " SLCopies, ODLearners, Notes, BuyingReason, RequestDate) " &
            "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

        Using con = New OleDb.OleDbConnection(dbProvider & dbsource)
            con.Open()
            Using cmd = New OleDbCommand(sql, con)
                With cmd.Parameters
                    .AddWithValue("Requester", NameTextBox.Text)

                    .AddWithValue("Email", EmailTextBox.Text)

                    .AddWithValue("Dept", Convert.ToInt32(DeptDropDown.SelectedValue))



                    .AddWithValue("Barcode", NumBarcodeTextBox.Text)

                    .AddWithValue("Author", MainAuthorTextBox.Text)

                    .AddWithValue("Title", BookTitleTextBox.Text)

                    .AddWithValue("Pub", PublisherTextBox.Text)


                    .AddWithValue("PubDate", YearPubTextBox.Text)

                    Dim isbn = ISBNBox.Text.Split(vbCrLf)

                    .AddWithValue("ISBN1", isbn(0))

                    If isbn.Length = 2 Then
                        .AddWithValue("ISBN2", isbn(1))
                    Else
                        .AddWithValue("ISBN2", "")
                    End If

                    .AddWithValue("IntendedLibrary", Convert.ToInt32(LibraryDropDown.SelectedValue))

                    .AddWithValue("Price", PriceTextBox.Text)

                    .AddWithValue("OLCopies", Convert.ToInt32(NoNewOrdinaryLoanTextBox.Text))

                    .AddWithValue("SLCopies", Convert.ToInt32(NoShortLoanBox.Text))

                    .AddWithValue("ODLearners", ODLearnersCheckBox.Checked)

                    .AddWithValue("Notes", NotesBox.Text)

                    .AddWithValue("BuyingReason", FindOutDropDown.Text)
                    .AddWithValue("RequestDate", Now)

                End With
                cmd.ExecuteNonQuery()
            End Using

            'Using da = New OleDb.OleDbDataAdapter("SELECT * FROM SUGGESTIONS", con)

            '    Dim ds As DataSet = New DataSet("Suggestions")
            '    da.Fill(ds)

            '    da.TableMappings.Add("Suggestions", "Suggestions")

            '    Dim dr As DataRow = ds.Tables(0).NewRow()

            '    dr("Requester") = NameTextBox.Text

            '    dr("Email") = EmailTextBox.Text

            '    dr("Dept") = DeptDropDown.SelectedValue



            '    dr("Barcode") = NumBarcodeTextBox.Text

            '    dr("Author") = MainAuthorTextBox.Text

            '    dr("Title") = BookTitleTextBox.Text

            '    dr("Pub") = PublisherTextBox.Text
            '    If Not String.IsNullOrEmpty(YearPubTextBox.Text) Then

            '        dr("PubDate") = DateValue(YearPubTextBox.Text)
            '    End If

            '    Dim isbn = ISBNBox.Text.Split(vbCrLf)

            '    dr("ISBN1") = isbn(0)

            '    If isbn.Length = 2 Then
            '        dr("ISBN2") = isbn(1)
            '    Else
            '        dr("ISBN2") = ""
            '    End If

            '    dr("IntendedLibrary") = LibraryDropDown.SelectedValue

            '    dr("Price") = PriceTextBox.Text

            '    dr("OLCopies") = NoNewOrdinaryLoanTextBox.Text

            '    dr("SLCopies") = NoShortLoanBox.Text

            '    dr("ODLearners") = ODLearnersCheckBox.Checked

            '    dr("Notes") = NotesBox.Text

            '    dr("BuyingReason") = FindOutDropDown.Text
            '    dr("RequestDate") = Now
            '    ds.Tables(0).Rows.Add(dr)
            '    Dim objCommandBuilder As New OleDbCommandBuilder(da)
            '    da.Update(ds, "Suggestions")
            'End Using
        End Using
    End Sub

    Private Sub WriteAlephRequest()

        Dim req As AlephConnector.NewACQRequest = CreateACQRequest()
        req.ServerName = My.Settings.AlephAddress
        req.ServerPort = My.Settings.AlephRestPort
        req.PatronNo = NumBarcodeTextBox.Text

        Dim resp = req.Send()
        StoreLibraryDetails()

        Dim message As New MailMessage()
        message.From = New MailAddress(My.Settings.FromAddress)
        message.To.Add(New MailAddress(My.Settings.ToAddress))
        message.Subject = "Book Suggestion from " & NameTextBox.Text & "(" & DeptDropDown.Text & ")"
        message.IsBodyHtml = True

        message.Body = GetEMailBody()
        Dim client As New SmtpClient
        client.Send(message)
        Response.Redirect("ack.aspx", False)
    End Sub

    Protected Sub FindButton_Click(sender As Object, e As EventArgs)


        Dim ac As New AlephConnector.Session(My.Settings.AlephAddress, My.Settings.AlephPort)
        Dim aq As New AlephConnector.AlephQuery()
        With aq
            .Title = BookTitleTextBox.Text
            .Author = MainAuthorTextBox.Text
            .Publisher = PublisherTextBox.Text
            .ISBN = ISBNBox.Text
            .AlephBase = My.Settings.AlephBase
        End With

        Try
            Dim res = ac.Search(aq)
            Dim details = res.DrillDown
            ResultsView.Visible = True

            If details.Record IsNot Nothing Then
                ResultsView.SetActiveView(AlephView)
                ResultsGrid.DataSource = details.Record
                ResultsGrid.DataBind()
                Session("results") = details
            Else
                ResultsView.SetActiveView(NullView)
            End If
        Catch ex As System.Net.WebException
            ResultsView.SetActiveView(ErrorView)
            MessageLabel.Text = ex.Message
        End Try

    End Sub

    Private Sub ResultsGrid_PageIndexChanged(sender As Object, e As EventArgs) Handles ResultsGrid.PageIndexChanged

    End Sub

    Private Sub ResultsGrid_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles ResultsGrid.PageIndexChanging

    End Sub


    Private Sub ResultsGrid_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles ResultsGrid.RowCommand
        If (e.CommandName = "UseDetails") Then
            ' Retrieve the row index stored in the CommandArgument property.
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)

            ' Retrieve the row that contains the button 
            ' from the Rows collection.
            Dim row As GridViewRow = ResultsGrid.Rows(index)

            Dim dlLabel As Label = row.FindControl("DocNumberLabel")
            Dim docNumber As String = dlLabel.Text
            Dim results As AlephConnector.Collection = Session("results")

            Dim specificResult = (From res As AlephConnector.CollectionRecord In results.Record
                                 Where res.DocNumber = docNumber
                                 Select res).FirstOrDefault()
            With specificResult
                BookTitleTextBox.Text = If(.Title, "")
                If .ISBN IsNot Nothing Then
                    ISBNBox.Text = String.Join(vbCrLf, .ISBN)
                End If
                If .Publisher IsNot Nothing Then
                    YearPubTextBox.Text = If(.Publisher.PubDate, "")
                    PublisherTextBox.Text = If(.Publisher.description, "")
                End If
                MainAuthorTextBox.Text = If(specificResult.Name, "")
            End With
            ResultsView.Visible = False
        End If
    End Sub

    Private Sub WriteEmailRow(hb As HtmlTextWriter, p2 As String, p3 As String)

        hb.RenderBeginTag("tr")

        hb.RenderBeginTag("td")
        hb.RenderBeginTag("b")
        hb.Write(p2)
        hb.RenderEndTag()
        hb.RenderEndTag()

        hb.RenderBeginTag("td")
        hb.Write(p3)
        hb.RenderEndTag()
    End Sub

    Protected Sub ClearButton_Click(sender As Object, e As EventArgs)
        MainAuthorTextBox.Text = ""
        ISBNBox.Text = ""
        BookTitleTextBox.Text = ""
        PublisherTextBox.Text = ""
        PriceTextBox.Text = ""
        YearPubTextBox.Text = ""

    End Sub


    Protected Sub FindAmazonButton_Click(sender As Object, e As EventArgs)

        Dim aq As New AmazonConnector.AmazonQuery()

        aq.Title = BookTitleTextBox.Text
        aq.Author = MainAuthorTextBox.Text
        aq.Publisher = PublisherTextBox.Text
        aq.ISBN = ISBNBox.Text
        Dim res = aq.Search()

        ResultsView.Visible = True

        If res IsNot Nothing Then
            ResultsView.SetActiveView(AmazonView)
            AmazonGridView.DataSource = res
            AmazonGridView.DataBind()
            Session("results") = res
        Else
            ResultsView.SetActiveView(NullView)
        End If
    End Sub

    Private Sub AmazonGridView_PageIndexChanged(sender As Object, e As EventArgs) Handles AmazonGridView.PageIndexChanged

    End Sub

    Private Sub AmazonGridView_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles AmazonGridView.PageIndexChanging

    End Sub

    Private Sub AmazonGridView_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles AmazonGridView.RowCommand
        If (e.CommandName = "UseDetails") Then
            ' Retrieve the row index stored in the CommandArgument property.
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)

            ' Retrieve the row that contains the button 
            ' from the Rows collection.
            Dim row As GridViewRow = AmazonGridView.Rows(index)

            Dim dlLabel As Label = row.FindControl("URL")
            Dim docURI As String = dlLabel.Text
            'get the most recent results out of session state
            Dim results As IEnumerable(Of AmazonConnector.AmazonResult) = Session("results")

            Dim specificResult = (From res As AmazonConnector.AmazonResult In results
                                 Where res.NavigateURL.OriginalString = docURI
                                 Select res).FirstOrDefault()
            With specificResult
                BookTitleTextBox.Text = If(.Title, "")
                If .ISBN IsNot Nothing Then
                    ISBNBox.Text = String.Join(vbCrLf, .ISBN)
                End If

                YearPubTextBox.Text = If(.Year, "")
                PublisherTextBox.Text = If(.Publisher, "")

                MainAuthorTextBox.Text = If(specificResult.Name, "")
                PriceTextBox.Text = If(specificResult.Price, "")
            End With
            ResultsView.Visible = False
        End If
    End Sub

    Private Sub StoreLibraryDetails()
        Dim cookie As HttpCookie
        cookie = Request.Cookies("Library")
        If cookie Is Nothing Then
            cookie = New HttpCookie("Library")
        End If
        cookie("Name") = LibraryDropDown.Text
        Response.Cookies.Add(cookie)
    End Sub

    Private Sub Suggestion_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim cookie As HttpCookie = Request.Cookies("Library")
        If Not cookie Is Nothing Then
            LibraryDropDown.Text = cookie("Name")
        End If
        LoadCombos()
    End Sub

    Private Sub LoadCombos()
        'TODO:  SP.NET process identity must be granted read and write file permissions to the Access databases stored in the App_Data folder. 

        Dim dbProvider As String = "PROVIDER=Microsoft.ACE.OLEDB.12.0;"
        Dim dbsource = "Data Source = " & Server.MapPath(My.Settings.DatabaseName)

        Dim deptData As New BookSuggsTableAdapters.DeptTableAdapter()

        deptData.Connection.ConnectionString = dbProvider & dbsource


        DeptDropDown.DataSource = deptData.GetData()
        DeptDropDown.DataValueField = "ID"
        DeptDropDown.DataTextField = "DeptName"
        DeptDropDown.DataBind()

        Dim libraryData As New BookSuggsTableAdapters.LibraryTableAdapter

        libraryData.Connection.ConnectionString = dbProvider & dbsource

       

        LibraryDropDown.DataSource = libraryData.GetData()
        LibraryDropDown.DataValueField = "ID"
        LibraryDropDown.DataTextField = "LibraryName"
        LibraryDropDown.DataBind()
    End Sub

    Private Function GetEMailBody() As String
        Dim sb As New System.Text.StringBuilder
        Dim sw As New System.IO.StringWriter(sb)
        Dim hb As New HtmlTextWriter(sw)
        hb.RenderBeginTag("html")
        hb.RenderBeginTag("body")
        hb.AddAttribute(HtmlTextWriterAttribute.Border, "solid")
        hb.RenderBeginTag("table")

        WriteEmailRow(hb, "Name:", NameTextBox.Text)

        WriteEmailRow(hb, "Dept:", DeptDropDown.Text)

        WriteEmailRow(hb, "Email:", EmailTextBox.Text)

        WriteEmailRow(hb, "Library Bar Code Number:", NumBarcodeTextBox.Text)

        WriteEmailRow(hb, "Main Author/Editor:", MainAuthorTextBox.Text)

        WriteEmailRow(hb, "Title:", BookTitleTextBox.Text)

        WriteEmailRow(hb, "Publisher:", PublisherTextBox.Text)

        WriteEmailRow(hb, "Publication Date:", YearPubTextBox.Text)

        WriteEmailRow(hb, "ISBN", ISBNBox.Text)

        WriteEmailRow(hb, "Intended E-Mail Library", LibraryDropDown.Text)

        WriteEmailRow(hb, "Price", PriceTextBox.Text)

        WriteEmailRow(hb, "No. of new copies:", NoNewOrdinaryLoanTextBox.Text)

        WriteEmailRow(hb, "No. of short loan copies:", NoShortLoanBox.Text)

        WriteEmailRow(hb, "For overseas/distance learners:", IIf(ODLearnersCheckBox.Checked, "Yes", "No"))

        WriteEmailRow(hb, "Notes:", NotesBox.Text)

        WriteEmailRow(hb, "How the reader found out about the book:", FindOutDropDown.Text)

        hb.RenderEndTag()
        hb.RenderEndTag()
        hb.RenderEndTag()

        Return sb.ToString()
    End Function

    Private Function CreateACQRequest() As AlephConnector.NewACQRequest
        Dim req As New AlephConnector.NewACQRequest
        req.Replytext = "ok"
        req.ReplyCode = "0000"
        req.ACQParameters = New AlephConnector.ACQParameter
        With req.ACQParameters
            .Allowed = "Y"
            .Reason = NotesBox.Text
            .Author = MainAuthorTextBox.Text
            .AddAuthor = ""
            .Title = BookTitleTextBox.Text
            .Edition = ""
            .PublicationPlace = ""
            .Publisher = PublisherTextBox.Text
            .PubYear = YearPubTextBox.Text
            .Series = ""
            .InfoSource = ""
            .ISBN = ISBNBox.Text
        End With
        Return req
    End Function

    Private Sub SubmitButton_Click1(sender As Object, e As EventArgs) Handles SubmitButton.Click

    End Sub

    Private Sub WriteAccessRequest2()
        Dim ta As New BookSuggsTableAdapters.SuggestionsTableAdapter
        Dim dbProvider As String = "PROVIDER=Microsoft.ACE.OLEDB.12.0;"
        Dim dbsource = "Data Source = " & Server.MapPath(My.Settings.DatabaseName)
        ta.Connection.ConnectionString = dbProvider & dbsource

        Dim isbn = ISBNBox.Text.Split(vbCrLf)
        Dim isbn2 As String
        Dim isbn1 = isbn(0)
        If isbn.Length = 2 Then
            isbn2 = isbn(1)
        Else
            isbn2 = ""
        End If
        ta.Insert(NameTextBox.Text, EmailTextBox.Text, Convert.ToInt32(DeptDropDown.SelectedValue),
                 NumBarcodeTextBox.Text, MainAuthorTextBox.Text, BookTitleTextBox.Text,
                 PublisherTextBox.Text, YearPubTextBox.Text, isbn1, isbn2, Convert.ToInt32(LibraryDropDown.SelectedValue),
                 PriceTextBox.Text, Convert.ToInt32(NoNewOrdinaryLoanTextBox.Text),
                 Convert.ToInt32(NoShortLoanBox.Text), ODLearnersCheckBox.Checked, NotesBox.Text, FindOutDropDown.Text, Now)


    End Sub

End Class