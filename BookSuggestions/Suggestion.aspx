﻿<%@ Page Title="" Language="vb" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="Suggestion.aspx.vb" Inherits="BookSuggestions.Suggestion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h1 runat="server">New Book Suggestion</h1>
    <asp:Label runat="server">Please complete this form in as much detail as possible.</asp:Label>
    <br />
    <asp:Label runat="server">Before suggesting a book: </asp:Label>
    <ul>
        <li>Please check <a href="http://aleph.nottingham.ac.uk/F?RN=785865169" title="Library Online catalogue">UNLOC</a>
        to ensure that the book is not already in stock. </li>
        <li>Click on <b>Find in Aleph</b> to locate your book in the Library - we might have it listed in the catalogue but may not have enough copies.</li>
        <li>Click on <b>Find in Amazon</b> to search for the book if we don't stock it</li>
    </ul>

    <hr />
    <asp:Table ID="FormTable" runat="server" Width="100%">
        <asp:TableRow>
            <asp:TableCell Style="padding-left: 15px; border-left-color: Black; border-left-style: solid; border-left-width: 3px;">
                    <asp:Label runat="server"><h3 runat="server">Your Details</h3></asp:Label>                
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow runat="server">
            <asp:TableCell runat="server">
                <asp:Label ID="NameLabel" runat="server">Name: <asp:Label runat="server" Font-Bold="true" ForeColor="Red">*</asp:Label> </asp:Label>
            </asp:TableCell>
            <asp:TableCell runat="server">
                <asp:TextBox ID="NameTextBox" runat="server" Columns="56"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow runat="server">
            <asp:TableCell runat="server">
                <asp:Label ID="EmailLabel" runat="server">E-Mail: <asp:Label runat="server" Font-Bold="true" ForeColor="Red">*</asp:Label> </asp:Label>

            </asp:TableCell>
            <asp:TableCell runat="server">
                <asp:TextBox ID="EmailTextBox" runat="server" Columns="56"></asp:TextBox>

            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow runat="server">
            <asp:TableCell runat="server">
                <asp:Label ID="Label1" runat="server" Text="Department"></asp:Label>
            </asp:TableCell>
            <asp:TableCell runat="server">
                <asp:DropDownList ID="DeptDropDown" runat="server">
                    
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="DeptValidator" runat="server" ControlToValidate="DeptDropDown" ErrorMessage="Department required."
                    InitialValue="select department" ForeColor="Red" />
            </asp:TableCell></asp:TableRow><asp:TableRow runat="server">
            <asp:TableCell runat="server">
                <asp:Label runat="server" ID="NumBarcodeLabel" Text="Library Number Barcode"></asp:Label>
            </asp:TableCell><asp:TableCell runat="server">
                <asp:TextBox runat="server" ID="NumBarcodeTextBox" Columns="56"></asp:TextBox>
                <asp:RegularExpressionValidator runat="server" ID="LibraryNoValidator" ErrorMessage="Your library code should be 10 digits" ValidationExpression="\d{10}" ControlToValidate="NumBarcodeTextBox" ForeColor="Red" />
            </asp:TableCell></asp:TableRow><asp:TableRow>
            <asp:TableCell Style="padding-left: 15px; border-left-color: Black; border-left-style: solid; border-left-width: 3px;">
                    <h3>Book Details</h3>                
            </asp:TableCell><asp:TableCell>
                <asp:Button runat="server" CausesValidation="false" Text="Find in Aleph" ID="FindButton" OnClick="FindButton_Click" />
                <asp:Button runat="server" CausesValidation="false" Text="Find in Amazon" ID="FindAmazonButton" OnClick="FindAmazonButton_Click" />
                <asp:Button runat="server" CausesValidation="false" Text="Clear" ID="ClearButton" OnClick="ClearButton_Click" />
            </asp:TableCell></asp:TableRow><asp:TableRow runat="server">
            <asp:TableCell runat="server" ID="resultsCell" ColumnSpan="2">
                <asp:Table runat="server" ID="resultsTable"></asp:Table>
            </asp:TableCell></asp:TableRow><asp:TableRow runat="server">
            <asp:TableCell runat="server">
                <asp:Label runat="server" ID="MainAuthorLabel" Text="Main Author/Editor"></asp:Label>
            </asp:TableCell><asp:TableCell runat="server">
                <asp:TextBox runat="server" ID="MainAuthorTextBox" Columns="56"></asp:TextBox>
            </asp:TableCell></asp:TableRow><asp:TableRow runat="server">
            <asp:TableCell runat="server">
                <asp:Label runat="server" ID="BookTitleLabel" Text="Book Title"></asp:Label>
            </asp:TableCell><asp:TableCell runat="server">
                <asp:TextBox runat="server" ID="BookTitleTextBox" Width="100%" Columns="80"></asp:TextBox>
            </asp:TableCell></asp:TableRow><asp:TableRow runat="server">
            <asp:TableCell runat="server">
                <asp:Label runat="server" ID="PublisherLabel" Text="Publisher"></asp:Label>
            </asp:TableCell><asp:TableCell runat="server">
                <asp:TextBox runat="server" ID="PublisherTextBox" Columns="56"></asp:TextBox>
            </asp:TableCell></asp:TableRow><asp:TableRow runat="server">
            <asp:TableCell runat="server">
                <asp:Label runat="server" ID="PubDateLabel" Text="Date of publication"></asp:Label>
            </asp:TableCell><asp:TableCell runat="server">
                <asp:TextBox ID="YearPubTextBox" runat="server"></asp:TextBox>

            </asp:TableCell></asp:TableRow><asp:TableRow runat="server">
            <asp:TableCell runat="server">
                <asp:Label runat="server" ID="ISBNLabel" Text="ISBN"></asp:Label>
            </asp:TableCell><asp:TableCell runat="server">
                <asp:TextBox ID="ISBNBox" TextMode="MultiLine" Columns="20" Rows="2" runat="server"></asp:TextBox>

            </asp:TableCell></asp:TableRow><asp:TableRow runat="server">
            <asp:TableCell runat="server" ColumnSpan="2">
                <asp:MultiView runat="server" ID="ResultsView" ActiveViewIndex="-1">
                    <asp:View ID="AlephView" runat="server">
                        <h2>Aleph Results</h2>
                        <asp:GridView
                            runat="server"
                            ID="ResultsGrid"
                            AutoGenerateColumns="false"
                            HeaderStyle-BackColor="LightGray"
                            AlternatingRowStyle-BackColor="WhiteSmoke">
                            <Columns>
                                <asp:BoundField Visible="false" DataField="doc_number" HeaderText="Doc ID" />
                                <asp:TemplateField Visible="false">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="DocNumberLabel" Text='<%#Bind("DocNumber")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="title" HeaderText="Title" />
                                <asp:TemplateField HeaderText="Author" SortExpression="Author">
                                    <ItemTemplate>
                                        <asp:Label ID="AuthName" runat="server"
                                            Text='<%#Bind("Name")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Publisher" SortExpression="Publisher">
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server"
                                            Text='<%#Bind("publisher.description")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Button ID="UseDetailsButton" runat="server" CausesValidation="false"
                                            CommandName="UseDetails"
                                            CommandArgument="<%# CType(Container,GridViewRow).RowIndex %>"
                                            Text="Suggest This" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </asp:View>
                    <asp:View ID="AmazonView" runat="server">
                        <h2>Amazon Results (only first 10 shown)</h2>
                        <asp:GridView
                            runat="server"
                            ID="AmazonGridView"
                            AutoGenerateColumns="false"
                            HeaderStyle-BackColor="LightGray"
                            AlternatingRowStyle-BackColor="WhiteSmoke">
                            <Columns>
                                <asp:TemplateField Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="URL" runat="server" Text='<%#Bind("NavigateURL")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Title">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="DetailsLink" runat="server" Text='<%#Bind("Title")%>' NavigateUrl='<%#Bind("NavigateURL") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Author" SortExpression="Author">
                                    <ItemTemplate>
                                        <asp:Label ID="AuthName" runat="server"
                                            Text='<%#Bind("Name")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Publisher" SortExpression="Publisher">
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server"
                                            Text='<%#Bind("Publisher")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Button ID="UseAmazonDetailsButton" runat="server" CausesValidation="false"
                                            CommandName="UseDetails"
                                            CommandArgument="<%# CType(Container,GridViewRow).RowIndex %>"
                                            Text="Suggest This" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </asp:View>
                    <asp:View ID="NullView" runat="server">
                        <asp:Label runat="server"><b>No results found. Try changing your search criteria to be less restrictive.</b></asp:Label>
                    </asp:View>
                    <asp:View ID="ErrorView" runat="server">
                        <asp:Label runat="server"><h3 style="color:red">Oops...something went wrong while searching Aleph</h3></asp:Label>
                        <asp:Label runat="server" ID="MessageLabel" Style="color: red" /><
                    </asp:View>
                </asp:MultiView>
            </asp:TableCell></asp:TableRow><asp:TableRow runat="server" />
        <asp:TableRow runat="server">
            <asp:TableCell runat="server" Style="padding-left: 15px; border-left-color: Black; border-left-style: solid; border-left-width: 3px;">
                    <asp:Label runat="server"><h3 runat="server">Request Details</h3></asp:Label>                
            </asp:TableCell></asp:TableRow><asp:TableRow runat="server">
            <asp:TableCell runat="server">What library is this intended for?</asp:TableCell><asp:TableCell runat="server">
                <asp:DropDownList runat="server" ID="LibraryDropDown">
                  
                </asp:DropDownList>
            </asp:TableCell></asp:TableRow><asp:TableRow runat="server">
            <asp:TableCell runat="server">
                <asp:Panel runat="server" ID="InStockPanel" Visible="false">
                    <h1>Blah Blah Blah</h1>
                </asp:Panel>
            </asp:TableCell></asp:TableRow><asp:TableRow runat="server">
            <asp:TableCell runat="server">
                <asp:Label runat="server" ID="PriceLabel" Text="Price"></asp:Label>
            </asp:TableCell><asp:TableCell runat="server">
                <asp:TextBox ID="PriceTextBox" runat="server" Columns="6"></asp:TextBox>
            </asp:TableCell></asp:TableRow><asp:TableRow runat="server">
            <asp:TableCell runat="server">
                <asp:Label runat="server" ID="NoNewCopiesLabel" Text="Number of new Ordinary Loan Copies"></asp:Label>
            </asp:TableCell><asp:TableCell>
                <asp:TextBox ID="NoNewOrdinaryLoanTextBox" Text="1" runat="server" Columns="3" />
                <asp:RangeValidator runat="server" ErrorMessage="Quantities must be between 1 and 100" ID="OrdinaryNoValidator" ControlToValidate="NoNewOrdinaryLoanTextBox" Type="Integer" MinimumValue="1" MaximumValue="100" ForeColor="Red" />
                <asp:RequiredFieldValidator runat="server" ID="OrdNoReqdValidator" ControlToValidate="NoNewOrdinaryLoanTextBox" ForeColor="Red" ErrorMessage="You have to request at least one book" />
            </asp:TableCell></asp:TableRow><asp:TableRow runat="server">
            <asp:TableCell runat="server">
                <asp:Label runat="server" ID="NoShortLoanLabel" Text="Number of new Short Loan Copies"></asp:Label>
            </asp:TableCell><asp:TableCell>
                <asp:TextBox ID="NoShortLoanBox" Text="1" runat="server" Columns="3" />
                <asp:RangeValidator runat="server" ErrorMessage="Quantities must be between 1 and 100" ID="RangeValidator1" ControlToValidate="NoShortLoanBox" Type="Integer" MinimumValue="1" MaximumValue="100" ForeColor="Red" />
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="NoShortLoanBox" ForeColor="Red" ErrorMessage="You have to request at least one book" />
            </asp:TableCell></asp:TableRow><asp:TableRow runat="server">
            <asp:TableCell runat="server">
                <asp:Label runat="server" ID="Label2" Text="For overseas/distance learners"></asp:Label>
            </asp:TableCell><asp:TableCell>
                <asp:CheckBox runat="server" ID="ODLearnersCheckBox" Checked="false" />
            </asp:TableCell></asp:TableRow><asp:TableRow runat="server">
            <asp:TableCell runat="server">
                <asp:Label runat="server" ID="Label3" Text="Notes"></asp:Label>
            </asp:TableCell><asp:TableCell>
                <asp:TextBox runat="server" ID="NotesBox" TextMode="MultiLine" Rows="6" Columns="45" />
            </asp:TableCell></asp:TableRow><asp:TableRow runat="server">
            <asp:TableCell runat="server">
                <asp:Label runat="server" ID="Label4" Text="How did you find out about this book?"></asp:Label>
            </asp:TableCell><asp:TableCell runat="server">
                <asp:DropDownList runat="server" ID="FindOutDropDown">
                    <asp:ListItem>Read a review</asp:ListItem>
                    <asp:ListItem>Borrowed a copy</asp:ListItem>
                    <asp:ListItem>Recommended by a colleague</asp:ListItem>
                    <asp:ListItem>Already on reading list</asp:ListItem>
                </asp:DropDownList>
            </asp:TableCell></asp:TableRow><asp:TableRow>
            <asp:TableCell>
                <asp:Button ID="SubmitButton" runat="server" Text="Submit" OnClick="SubmitButton_Click" />
            </asp:TableCell></asp:TableRow></asp:Table></asp:Content>